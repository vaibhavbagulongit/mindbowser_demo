import React, {Component} from 'react';
import {Text, View, Button} from 'react-native';
import styles from './ListStyle';

class welcome extends Component {
  constructor(props) {
    super(props);
    this.state = {
      favList: [],
    };
  }

  render() {
    return (
      <View style={styles.welcomeContainer}>
        <View style={styles.innterContainer}>
          <Text style={styles.welcomeText}>Welcome!</Text>
          <Text style={styles.mindbowser}>Mindbowser</Text>
          <Button
            title={'Click to view list'}
            onPress={() => this.props.navigation.navigate('Gif List')}
            color="#00BFFF"
          />
        </View>
      </View>
    );
  }
}

export default welcome;
