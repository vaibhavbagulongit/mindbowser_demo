import React, { Component } from 'react';
import ListView from './ListView';
import styles from './ListStyle';
import {
    View,
    Text,
    TouchableOpacity, Image, TouchableHighlight, ToastAndroid,AsyncStorage
} from "react-native";
import { Card } from 'react-native-paper';

const dislikeIcon = require('../image/dislike.png');
const likeIcon = require('../image/like.png');

class ApiContainer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            dataSource: [],
            favList: [],
            APIKey: '7tXLFBCY7webeJV61Bh8djcc4AzahIUd',
            limit: 20,
            totalCount: 0,
            offset: 0,
            term: 'adventure+time',
            loading : true

        };
    }

    componentDidMount()
    {
        this.goForFetch();
    }

    _onPressBtn = (id) => {
        let index = this.state.favList.indexOf(id);
        if (index === -1) {
            if (this.state.favList.length <= 4) {
                this.setState({ favList: [...this.state.favList, id] })
            }
            else {
                ToastAndroid.showWithGravity(
                    'You can only choose maximum 5 items as favorites.', ToastAndroid.SHORT, ToastAndroid.BOTTOM);
            }

        }
        else {
            var array = [...this.state.favList];
            array.splice(index, 1)
            this.setState({ favList: array });
        }
    }

    navigatetodetail = async(data) =>
    {
        AsyncStorage.setItem('name', JSON.stringify(this.state.favList));
        this.props.navigation.navigate('Gif Details',{data: data.item})
    }

    goForFetch = () => {

       
        if(this.state.totalCount === 0 || this.state.offset < this.state.totalCount)
        {
            const url = "https://api.giphy.com/v1/gifs/search?api_key="+this.state.APIKey+"&q="+this.state.term+"&limit="+this.state.limit+"&offset="+this.state.offset+"&rating=G&lang=en"
            console.log(url)
            fetch(url, { method: 'GET' })
            .then(response => response.json())
            .then((responseJson) => {
            
                // console.log("Response ===>" , JSON.stringify(responseJson))

                const { pagination } = responseJson;
                setTimeout(() => {
                    this.setState({
                        dataSource: this.state.dataSource.concat(responseJson.data),
                        totalCount: pagination.total_count,
                        offset: pagination.offset + pagination.count,
                        loading:false
                      });

                }, 500)

            })
            .catch(error => console.log(error))
        }
        else
        {
            console.log('End of results');
        }
        
    }

    FlatListSeparator = () => {
        return (
            <View style={{
                height: .5,
                width: "100%",
                backgroundColor: "rgba(0,0,0,0.5)",
            }}
            />
        );
    }
    renderItem = (data) => {
        let icon = this.state.favList.indexOf(data.item.id) === -1 ? dislikeIcon : likeIcon;
        return (
            <Card style={styles.card}>

                <View style={{justifyContent: "center",alignItems: "center",}}>
                <TouchableOpacity style={styles.list} onPress={() => this.navigatetodetail(data)}
                >
                    <Image style={styles.gif}
                        source={{ uri: data.item.images.original.url }}
                    />
                </TouchableOpacity>
                </View>
                <View style={styles.Layoutview}>
                    <Text style={styles.textStyle}>{data.item.title}</Text>
                    <TouchableHighlight
                        onPress={() => this._onPressBtn(data.item.id)}
                        underlayColor="#fefefe"
                    >
                        <Image style={{marginTop:10,marginBottom:10}}
                            source={icon}
                        />
                    </TouchableHighlight>
                </View>

            </Card>
        )
    }

    render() {

        const { dataSource, fromFetch, fromAxios, loading, axiosData } = this.state
        return (
            <ListView
                goForFetch={this.goForFetch}
                dataSource={dataSource}
                loading={loading}
                fromFetch={fromFetch}
                FlatListSeparator={this.FlatListSeparator}
                renderItem={this.renderItem}
            />
        );
    }
}

export default ApiContainer;