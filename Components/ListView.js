import React, { Component } from 'react'
import { View, Text, Button, FlatList, ActivityIndicator } from 'react-native';
import Spinner from 'react-native-loading-spinner-overlay'
import styles from './ListStyle';


const ListView = (props) => {
    const { goForFetch, renderItem, FlatListItemSeparator, dataSource, loading } = props
    return (
        <View style={styles.parentContainer}>    
                <FlatList
                    data={dataSource}
                    ItemSeparatorComponent={FlatListItemSeparator}
                    renderItem={item => renderItem(item)}
                    keyExtractor={item => item.id.toString()}
                    onEndReached={() => {goForFetch}}
                    contentContainerStyle={{ paddingBottom: 150}}
                /> 
            {<Spinner 
            visible={loading}
            textContent={'Loding...'}
            textStyle={{color:'#fff'}}
            />}
        </View>
    )
}
export default ListView;
